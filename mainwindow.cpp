/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtDebug>

#include <Settings/usersettings.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("BatMan - Battery Manager");
    setWindowIcon(QIcon(":/icons/Tux-BatMan icon 512.png"));

    connect(ui->actionAbout, &QAction::triggered, &mAboutDialog, &AboutDialog::show);
    connect(ui->actionQuit, &QAction::triggered, QCoreApplication::instance(), &QCoreApplication::quit, Qt::QueuedConnection);


    connect(ui->pBtnPersMode, &QPushButton::clicked, this, &MainWindow::showPersistentModeEditDialog);
    connect(ui->pBtnSchedMode, &QPushButton::clicked, this, &MainWindow::showScheduledModeEditorDialog);

    ui->pGrid->setTitle("Grid");
    ui->pGrid->setSignedMaxValue(20000);
    ui->pGrid->setReverseSign(true);
    ui->pGrid->setSignedUnit("kW", 1000);

    ui->pPowerBalance->setTitle("Balans");
    ui->pPowerBalance->setLeftMaxValue(15000);
    ui->pPowerBalance->setLeftUnit("kW", 1000);
    ui->pPowerBalance->setRightMaxValue(15000);
    ui->pPowerBalance->setRightUnit("kW", 1000);

    ui->pBattery->setTitle("Batterier");
    ui->pBattery->setSignedMaxValue(15000);
    ui->pBattery->setSignedUnit("kW", 1000);

    ui->pPriceWidget->updatePrices();

    ui->pBatteryBar->setTitle("Laddning");
    ui->pBatteryBar->setMaxValue(100);
    ui->pBatteryBar->setUnit("%", 1);


    connect(ui->actionEdit_Settings, &QAction::triggered, &mSettings, &UserSettings::show);

    connect(&mSettings, &UserSettings::needToShowSettingsDialog, &mSettings, &UserSettings::show);
    connect(&mSettings, &UserSettings::settingsLoaded, this, &MainWindow::connectToServices);

    mSettings.checkAndLoadSettings();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::dataRecevied(QVariantMap data)
{
    printData(data);

    ui->pGrid->setSignedValue(data.value("Ac Source, Total Active Power"));

    ui->pPowerBalance->setLeftValue(data.value("AC-Load, Total Power"));
    ui->pPowerBalance->setRightValue(data.value("Solar power"));

    ui->pBattery->setSignedValue(data.value("Battery Common, ChargingPower"));
    ui->pBatteryBar->setValue(data.value("Battery Common, State of Charge"));

}


void MainWindow::printData(QVariant data)
{
    Q_UNUSED(data)
    // qDebug() << "Got data: " << data;
}

void MainWindow::showPersistentModeEditDialog()
{
    if (mPersistentModeEditor.isNull())
    {
        mPersistentModeEditor = mModesController->persistentModeEditor(this);
        connect(mModesController.data(), &ModesController::destroyed, mPersistentModeEditor.data(), &QDialog::close);
        connect(mModesController.data(), &ModesController::destroyed, mPersistentModeEditor.data(), &QDialog::deleteLater);
    }

    mPersistentModeEditor->show();

}

void MainWindow::showScheduledModeEditorDialog()
{
    if (mScheduledModeEditor.isNull())
    {
        mScheduledModeEditor = mModesController->scheduledModeEditor(this);
        connect(mModesController.data(), &ModesController::destroyed, mScheduledModeEditor.data(), &QDialog::close);
        connect(mModesController.data(), &ModesController::destroyed, mScheduledModeEditor.data(), &QDialog::deleteLater);
    }

    mScheduledModeEditor->show();

}

void MainWindow::connectToServices()
{
    qDebug() << "Connect to services:" ;
    if (mPubSubClient)
        delete mPubSubClient;

    mPubSubClient = new PubSubClient(mSettings.daemonHost(), mSettings.pubSubPort().toInt(), this);
    connect(mPubSubClient.data(), &PubSubClient::dataRecevied, this, &MainWindow::dataRecevied);

    mPubSubClient->connectToProxy();

    if (mModesController)
        delete mModesController;

    mModesController = new ModesController(mSettings.daemonHost(), mSettings.managerPort().toInt(), ui->pCurrentManagerSettings, this);
    mModesController->connectManager();
}


