/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "aboutdialog.h"
#include "pubsubclient.h"

#include <QMainWindow>
#include <QPointer>
#include <QTimer>

#include <modes/modescontroller.h>

#include <Settings/usersettings.h>

QT_BEGIN_NAMESPACE

namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    public slots:

        void dataRecevied(QVariantMap data);
        void printData(QVariant data);

        void showPersistentModeEditDialog();
        void showScheduledModeEditorDialog();

    protected slots:

        void connectToServices();


    private:
        Ui::MainWindow *ui;

        QPointer<PubSubClient>              mPubSubClient;
        QPointer<ModesController>           mModesController;
        QPointer<QDialog>                   mPersistentModeEditor;
        QPointer<QDialog>                   mScheduledModeEditor;

        UserSettings                        mSettings;
        AboutDialog                         mAboutDialog;


};
#endif // MAINWINDOW_H
