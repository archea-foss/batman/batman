/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PRICETIMESLOT_H
#define PRICETIMESLOT_H

#include <QDateTime>

class PriceTimeSlot
{
    public:
        explicit PriceTimeSlot(QDateTime from,
                               QDateTime to,
                               qreal sek);

        PriceTimeSlot(const PriceTimeSlot& other);
        ~PriceTimeSlot();

        PriceTimeSlot& operator = (const PriceTimeSlot& other);

        qreal sek() const;
        QDateTime from() const;
        QDateTime to() const;

    protected:

        qreal       mSEK;
        QDateTime   mFrom;
        QDateTime   mTo;

};

#endif // PRICETIMESLOT_H
