/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "pricediagramwidget.h"
#include "ui_pricediagramwidget.h"

#include "elprisetjustnuproxy.h"
#include "pricetimelinerequest.h"

#include <QDate>
#include <QTime>
#include <QDateTime>

PriceDiagramWidget::PriceDiagramWidget(QWidget *parent)
    : QWidget(parent)
    , mReferenceTime(QDate::currentDate(), QTime(0, 0))
    , ui(new Ui::PriceDiagramWidget)

{
    ui->setupUi(this);
    connect(ui->pUpdate, &QPushButton::clicked, this, &PriceDiagramWidget::updatePrices);
}

PriceDiagramWidget::~PriceDiagramWidget()
{
    delete ui;
}

void PriceDiagramWidget::updatePrices()
{
    mReferenceTime = QDateTime(QDate::currentDate(), QTime(0,0));
    getPrices();
}

void PriceDiagramWidget::getPrices()
{

    PriceTimelineRequest    *request;

    request = new PriceTimelineRequest(mReferenceTime.date().addDays(-1), "SE4", &mProxy, this);
    connect(request, &PriceTimelineRequest::priceTimeLine, this, &PriceDiagramWidget::setYesterdaysPrices);
    connect(request, &PriceTimelineRequest::done, request, &PriceTimelineRequest::deleteLater);
    request->post();

}

PriceTimeLine PriceDiagramWidget::tomorrowsPrices() const
{
    return mTomorrowsPrices;
}


void PriceDiagramWidget::reconsiderGraph()
{
    // Draw today and tomorrow if feasible

    // qDebug() << "Yesterday    : " << mYesterdaysPrices.count();
    // qDebug() << "Today        : " << mTodaysPrices.count();
    // qDebug() << "Tomorrow     : " << mTomorrowsPrices.count();

    if (mTodaysPrices.count() == 24 && mTomorrowsPrices.count() == 24)
    {
        PriceTimeLine knownDays(mTodaysPrices);
        knownDays.append(mTomorrowsPrices);
        ui->pView->clearTimeLine();
        ui->pView->addTimeLine(knownDays, true);
    }
    else if (mTodaysPrices.count() == 24 && mYesterdaysPrices.count() == 24)
    {
        PriceTimeLine knownDays(mYesterdaysPrices);
        knownDays.append(mTodaysPrices);
        ui->pView->clearTimeLine();
        ui->pView->addTimeLine(knownDays, false);
    }

}

PriceTimeLine PriceDiagramWidget::todaysPrices() const
{
    return mTodaysPrices;
}


PriceTimeLine PriceDiagramWidget::yesterdaysPrices() const
{
    return mYesterdaysPrices;
}

void PriceDiagramWidget::setYesterdaysPrices(const PriceTimeLine &newYesterdaysPrices)
{
    mYesterdaysPrices = newYesterdaysPrices;

    PriceTimelineRequest    *request;
    request = new PriceTimelineRequest(mReferenceTime.date(), "SE4", &mProxy, this);
    connect(request, &PriceTimelineRequest::priceTimeLine, this, &PriceDiagramWidget::setTodaysPrices);
    connect(request, &PriceTimelineRequest::done, request, &PriceTimelineRequest::deleteLater);
    request->post();

}

void PriceDiagramWidget::setTodaysPrices(const PriceTimeLine &newTodaysPrices)
{
    mTodaysPrices = newTodaysPrices;

    PriceTimelineRequest    *request;
    request = new PriceTimelineRequest(mReferenceTime.date().addDays(1), "SE4", &mProxy, this);
    connect(request, &PriceTimelineRequest::priceTimeLine, this, &PriceDiagramWidget::setTomorrowsPrices);
    connect(request, &PriceTimelineRequest::done, this, &PriceDiagramWidget::reconsiderGraph);
    connect(request, &PriceTimelineRequest::done, request, &PriceTimelineRequest::deleteLater);
    request->post();

}

void PriceDiagramWidget::setTomorrowsPrices(const PriceTimeLine &newTomorrowsPrices)
{
    mTomorrowsPrices = newTomorrowsPrices;
}




