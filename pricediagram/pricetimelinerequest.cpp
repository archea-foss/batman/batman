/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "pricetimelinerequest.h"

#include "elprisetjustnuproxy.h"
#include <QNetworkReply>

PriceTimelineRequest::PriceTimelineRequest(QDate date, QString area, ElprisetjustnuProxy *proxy, QObject *parent)
    : QObject{parent}
    , mDate(date)
    , mArea(area)
    , mProxy(proxy)
{
    connect(mProxy.data(), &ElprisetjustnuProxy::finished, this, &PriceTimelineRequest::finished);
    connect(mProxy.data(), &ElprisetjustnuProxy::error, this, &PriceTimelineRequest::error);

}

QDate PriceTimelineRequest::date() const
{
    return mDate;
}

QString PriceTimelineRequest::area() const
{
    return mArea;
}

QByteArray PriceTimelineRequest::replyData() const
{
    return mReplyData;

}

QNetworkReply::NetworkError PriceTimelineRequest::post()
{
    return mProxy->fetch(this);
}

void PriceTimelineRequest::error(QNetworkReply *reply)
{
    if (reply && reply->error() != QNetworkReply::NoError)
        qDebug() << "Got an error: " << reply->errorString();

    emit done();
}


void PriceTimelineRequest::finished(QNetworkReply *reply)
{
    // qDebug() << "Answer";
    mReplyData = reply->readAll();
    PriceTimeLine timeline = mProxy->parseResult(this);
    emit priceTimeLine(timeline);
    emit done();

}
