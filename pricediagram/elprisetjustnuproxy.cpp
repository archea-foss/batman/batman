/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "elprisetjustnuproxy.h"
#include "pricetimelinerequest.h"

#include <QVariant>
#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>
#include <QUrl>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QDate>
#include <QDateTime>

#include <QtDebug>

ElprisetjustnuProxy::ElprisetjustnuProxy(QObject *parent)
    : QObject{parent}
{

}

QNetworkReply::NetworkError ElprisetjustnuProxy::fetch(PriceTimelineRequest *pRequest)
{

    QUrl        baseUrl(QString("https://www.elprisetjustnu.se/api/v1/prices/%1_%2.json")
                        .arg(pRequest->date().toString("yyyy/MM-dd"))
                        .arg(pRequest->area()));

    // qDebug() << "Url : " << baseUrl;
    // qDebug() << "Port: " << baseUrl.port();



    QNetworkRequest         request(baseUrl);


    QNetworkAccessManager*  nam = new QNetworkAccessManager(this);

    connect(nam, &QNetworkAccessManager::finished, this, &ElprisetjustnuProxy::finished);
    connect(nam, &QNetworkAccessManager::sslErrors, this, &ElprisetjustnuProxy::sslErrors);
    connect(nam, &QNetworkAccessManager::encrypted, this, &ElprisetjustnuProxy::encrypted);
    connect(nam, &QNetworkAccessManager::sslErrors, this, &ElprisetjustnuProxy::error);


    QNetworkReply *reply = nam->get(request);

    Q_ASSERT(reply != nullptr);

    return reply->error();


}

PriceTimeLine ElprisetjustnuProxy::parseResult(PriceTimelineRequest *pRequest)
{

    PriceTimeLine timeLine;

    QJsonDocument doc = QJsonDocument::fromJson(pRequest->replyData());
    QJsonArray priceArray = doc.array();

    for(QJsonValue value : priceArray)
    {
        QJsonObject priceObject = value.toObject();

        QDateTime from  = QDateTime::fromString(priceObject.value("time_start").toString(), Qt::ISODate);
        QDateTime to    = QDateTime::fromString(priceObject.value("time_end").toString(), Qt::ISODate);
        qreal     price = priceObject.value("SEK_per_kWh").toDouble();


        // qDebug() << "From  : " << from.toString("yyyy-MM-dd hh:mm");
        // qDebug() << "To    : " << to  .toString("yyyy-MM-dd hh:mm");
        // qDebug() << "Price : " << price;

        timeLine.append(PriceTimeSlot(from, to, price));

    }

    return timeLine;

}

