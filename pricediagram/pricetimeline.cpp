/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "pricetimeline.h"

PriceTimeLine::PriceTimeLine()
{

}

PriceTimeLine::PriceTimeLine(const PriceTimeLine &other)
    : mTimeSlots(other.mTimeSlots)
    , mFirstTime(other.mFirstTime)
    , mLastTime(other.mLastTime)
{

}

PriceTimeLine::~PriceTimeLine()
{

}

PriceTimeLine &PriceTimeLine::operator =(const PriceTimeLine &other)
{
    mTimeSlots = other.mTimeSlots;
    mFirstTime = other.mFirstTime;
    mLastTime  = other.mLastTime;

    return *this;
}

void PriceTimeLine::append(PriceTimeSlot slot)
{
    mTimeSlots.append(slot);
    if (mFirstTime.isNull() || slot.from() < mFirstTime)
        mFirstTime = slot.from();

    if (mLastTime.isNull() || slot.to() > mLastTime)
        mLastTime = slot.to();

}

void PriceTimeLine::append(PriceTimeLine timeLine)
{
//    mTimeSlots.append(timeLine.mTimeSlots);

    for(PriceTimeSlot slot : timeLine.mTimeSlots)
    {
        append(slot);
    }

}

int PriceTimeLine::count() const
{
    return mTimeSlots.count();
}

QList<QPair<QDateTime, qreal>> PriceTimeLine::hourlyPrices(QDateTime from, QDateTime to, qreal &minValue, qreal &maxValue, qreal gridFee)
{

    maxValue = 0;
    minValue = 100000;

    QList<QPair<QDateTime, qreal>>    result;

    foreach(PriceTimeSlot slot, mTimeSlots)
    {
        if (slot.from() >= from && slot.to() <= to)
        {
            QPair<QDateTime, qreal> priceValue;
            priceValue.first  = slot.from();
            priceValue.second = slot.sek() + gridFee;

            result.append(priceValue);

            if (slot.sek() > maxValue)
                maxValue = slot.sek();

            if (slot.sek() < minValue)
                minValue = slot.sek();

        }
    }

    return result;
}

QList<QPair<QDateTime, qreal> > PriceTimeLine::hourlyPrices(qreal &minValue, qreal &maxValue, qreal gridFee)
{
    maxValue = 0;
    minValue = 100000;

    QList<QPair<QDateTime, qreal>>    result;

    foreach(PriceTimeSlot slot, mTimeSlots)
    {
        QPair<QDateTime, qreal> priceValue;
        priceValue.first  = slot.from();
        priceValue.second = slot.sek() + gridFee;

        result.append(priceValue);

        if (slot.sek() > maxValue)
            maxValue = slot.sek();

        if (slot.sek() < minValue)
            minValue = slot.sek();
    }

    return result;
}
