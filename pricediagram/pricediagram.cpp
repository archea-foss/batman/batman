/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "pricediagram.h"

#include <QDateTime>

#include <QColor>
#include <QPaintEvent>
#include <QPainter>
#include <QPainterPath>
#include <QtMath>

#include <QtDebug>

PriceDiagram::PriceDiagram(QWidget *parent)
    : QWidget{parent}
    , mMinValue(1)
    , mMaxValue(1)
    , mGridRange(1)
    , mFloor(0)
    , mGrids(10)
    , mSecondIsTomorrow(false)
{

}

void PriceDiagram::addTimeLine(PriceTimeLine &timeLine, bool secondIsTomorrow)
{
    // qDebug() << "Adding to the timeline";

    mSecondIsTomorrow = secondIsTomorrow;

    mValues = timeLine.hourlyPrices(mMinValue, mMaxValue /*, 0.15 + 0.3920*/);

    // qDebug() << "Using " << mValues.count() << " to draw the diagram";

    mGridRange = mMaxValue - mMinValue;

    if (mGridRange <= 0.1)
    {
        mGridRange = 0.1;
        mGrids     = 5;
    }
    else if(mGridRange <= 0.3)
    {
        mGridRange = 0.3;
        mGrids     = 3;
    }
    else if(mGridRange <= 0.5)
    {
        mGridRange = 0.5;
        mGrids     = 5;
    }
    else if(mGridRange <= 1)
    {
        mGridRange = 1;
        mGrids     = 4;
    }
    else if(mGridRange <= 1.5)
    {
        mGridRange = 1.5;
        mGrids     = 6;

    }
    else if(mGridRange <= 2)
    {
        mGridRange = 2;
        mGrids     = 8;

    }
    else if(mGridRange <= 2.5)
    {
        mGridRange = 2.5;
        mGrids     = 6;
    }
    else
    {
        // On higher values we round uppwards to every 0.5 SEK.
        mGridRange = qCeil(mGridRange * 2) * 0.5;

        // The gridd will be every 0.5 SEK.
        mGrids     = mGridRange * 2;
    }


    // for(QPair<QDateTime, qreal> value : mValues)
    // {
    //     qDebug() << value.first.toString("yyyy-MM-dd hh:mm") << "    " << value.second;
    // }


    // Calculate the floor
    qreal resolution = mGridRange / mGrids;
    qreal divider    = mMinValue / resolution;
    mFloor           = qFloor(divider) * resolution;


    // qDebug() << "Resultion " << resolution;
    // qDebug() << "Divider " << divider;
    // qDebug() << "Floor " << mFloor;

    // qDebug() << "Range "    << mGridRange;
    // qDebug() << "Steps "    << mGrids;
    // qDebug() << "Min Value" << mMinValue;
    // qDebug() << "Max Value" << mMaxValue;
    // qDebug() << "Diagram max " << mFloor + mGridRange;

    // Verify that values are in range.
    if ( mFloor + mGridRange < mMaxValue)
    {
        // It is not. Add an extra step on the Y axis.
        mGridRange += mGridRange / mGrids;
        mGrids ++;
    }

    // qDebug() << "\nAdjusted values";
    // qDebug() << "Range "    << mGridRange;
    // qDebug() << "Steps "    << mGrids;
    // qDebug() << "Min Value" << mMinValue;
    // qDebug() << "Max Value" << mMaxValue;
    // qDebug() << "Diagram max " << mFloor + mGridRange;

    update();

}

void PriceDiagram::clearTimeLine()
{
    mValues.clear();
}

void PriceDiagram::paintEvent(QPaintEvent *event)
{

//    qDebug() << "Paints the diagram";
    event->accept();

    if (mValues.isEmpty())
    {
//        qDebug() << "No values to paint";
        return;
    }

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);


    qreal   height = this->size().height() - 40;
    qreal   width= this->size().width() - 40;

    qreal   xDelta = width / mValues.size();
    qreal   yDelta = height / mGridRange;

    qreal   yOffset = 10;

    qreal   x = 30;

    // White baground.
    painter.fillRect(0, 0, this->size().width(), this->size().height(), Qt::white);

    // Fade background some on not today.
    if (mValues.count() > 24)
    {
        if (mSecondIsTomorrow)
        {
            painter.fillRect(x + width / 2, 0, x + width / 2, this->size().height(), QColor("whitesmoke"));
        }
        else
        {
            painter.fillRect(x, 0, width / 2, this->size().height(), QColor("whitesmoke"));
        }

    }


    // Draw Grid

    QPen    grid(Qt::darkGray);
    grid.setWidthF(0.2);
    painter.setPen(grid);

    for (float j = 0; j < mGridRange; j += mGridRange / mGrids)
    {
        QLineF      bar(x, yOffset + height - j * yDelta, width + x, yOffset + height - j * yDelta);
        painter.drawLine(bar);
        painter.drawText(QPointF(3, yOffset + height - j * yDelta + 5), QString().asprintf("%0.2f", j + mFloor));
    }

    // Dra the upper bar not included above.
    QLineF      bar(x, yOffset, width + x, yOffset);
    painter.drawLine(bar);
    painter.drawText(QPointF(3, yOffset + 5), QString().asprintf("%0.2f", mGridRange + mFloor));


    // Plot the data and the vertical bars

    QFontMetricsF   fontMetrics(painter.font(), this);

    QPainterPath    path;

    bool bIsFirst = true;

    for(QPair<QDateTime, qreal> value : mValues)
    {
        // Add the datapoint to the path.
        if (bIsFirst)
        {
            path.moveTo(x, yOffset + height - (value.second - mFloor)* yDelta);
            path.lineTo(x + xDelta, yOffset + height - (value.second - mFloor) * yDelta);
            bIsFirst = false;
        }
        else
        {
            path.lineTo(x, yOffset + height - (value.second - mFloor)* yDelta);
            path.lineTo(x + xDelta, yOffset + height - (value.second - mFloor) * yDelta);
        }

        // Draw the vertical bar.
        painter.drawLine(x, yOffset, x, yOffset + height);


        // Draw the lable.
        QString lable = QString("%1").arg(value.first.time().hour(), -2);

        qreal lableW = fontMetrics.size(Qt::TextSingleLine, lable).width();

        if (lableW > xDelta)
        {
            if (value.first.time().hour() % 2 == 0)
            {
                painter.drawText(x + (xDelta - lableW) / 2, yOffset + height + 20, lable);
            }
        }
        else
        {
            painter.drawText(x + (xDelta - lableW) / 2, yOffset + height + 20, lable);
        }

        x += xDelta;

    }

    // Draw the last vertical bar.
    painter.drawLine(x, yOffset, x, yOffset + height);



    QColor linePenColor("royalblue");
    QPen    linePen(linePenColor);
    linePen.setWidthF(2);
    painter.setPen(linePen);
    painter.drawPath(path);

}


