/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PRICETIMELINE_H
#define PRICETIMELINE_H

#include "pricetimeslot.h"

#include <QList>

class PriceTimeLine
{
    public:
        PriceTimeLine();
        PriceTimeLine(const PriceTimeLine& other);
        ~PriceTimeLine();

        PriceTimeLine& operator = (const PriceTimeLine& other);


        void append(PriceTimeSlot slot);
        void append(PriceTimeLine timeLine);

        int count() const;

        QList<QPair<QDateTime, qreal> > hourlyPrices(QDateTime from, QDateTime to, qreal &minValue, qreal &maxValue, qreal gridFee = 0);
        QList<QPair<QDateTime, qreal> > hourlyPrices(qreal &minValue, qreal &maxValue, qreal gridFee = 0);

    protected:

        QList<PriceTimeSlot>        mTimeSlots;
        QDateTime                   mFirstTime;
        QDateTime                   mLastTime;


};

#endif // PRICETIMELINE_H
