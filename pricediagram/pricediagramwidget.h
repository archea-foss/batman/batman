/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PRICEDIAGRAMWIDGET_H
#define PRICEDIAGRAMWIDGET_H

#include "elprisetjustnuproxy.h"

#include <QPointer>
#include <QWidget>

namespace Ui {
class PriceDiagramWidget;
}

class PriceTimelineRequest;

class PriceDiagramWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit PriceDiagramWidget(QWidget *parent = nullptr);
        ~PriceDiagramWidget();

        PriceTimeLine yesterdaysPrices() const;

        PriceTimeLine todaysPrices() const;

        PriceTimeLine tomorrowsPrices() const;

    public slots:

        void updatePrices();


    protected slots:

        void getPrices();

        void setYesterdaysPrices(const PriceTimeLine &newYesterdaysPrices);
        void setTodaysPrices(const PriceTimeLine &newTodaysPrices);
        void setTomorrowsPrices(const PriceTimeLine &newTomorrowsPrices);

        void reconsiderGraph();






    protected:
        ElprisetjustnuProxy                 mProxy;
        PriceTimeLine                       mYesterdaysPrices;
        PriceTimeLine                       mTodaysPrices;
        PriceTimeLine                       mTomorrowsPrices;
        QList<QPair<QDateTime, qreal>>      mValues;
        QDateTime                           mReferenceTime;

    private:
        Ui::PriceDiagramWidget *ui;
};

#endif // PRICEDIAGRAMWIDGET_H
