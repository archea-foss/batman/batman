/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef ELPRISETJUSTNUPROXY_H
#define ELPRISETJUSTNUPROXY_H

#include <QNetworkReply>
#include <QObject>
#include <QSslError>

#include "pricetimeline.h"

class PriceTimelineRequest;

class ElprisetjustnuProxy : public QObject
{
        Q_OBJECT
    public:
        explicit ElprisetjustnuProxy(QObject *parent = nullptr);
        QNetworkReply::NetworkError fetch(PriceTimelineRequest* pRequest);
        PriceTimeLine parseResult(PriceTimelineRequest* pRequest);

    signals:
        void finished(QNetworkReply *reply);

        void sslErrors(QNetworkReply *reply, const QList<QSslError> &errors);
        void encrypted(QNetworkReply *reply);
        void error(QNetworkReply* reply);

};

#endif // ELPRISETJUSTNUPROXY_H
