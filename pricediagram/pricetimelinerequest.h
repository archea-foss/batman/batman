/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PRICETIMELINEREQUEST_H
#define PRICETIMELINEREQUEST_H

#include "pricetimeline.h"

#include <QDate>
#include <QNetworkReply>
#include <QObject>
#include <QPointer>
#include <QSslError>
#include <QString>

class ElprisetjustnuProxy;

class PriceTimelineRequest : public QObject
{
        Q_OBJECT
    public:
        explicit PriceTimelineRequest(QDate date, QString area, ElprisetjustnuProxy* proxy, QObject *parent = nullptr);


        QDate date() const;
        QString area() const;
        QByteArray replyData() const;

    signals:

        void priceTimeLine(PriceTimeLine& timeline);
        void done();

    public slots:
        QNetworkReply::NetworkError post();

    protected slots:
        void error(QNetworkReply *reply);
        void finished(QNetworkReply *reply);

    private:
        QDate                           mDate;
        QString                         mArea;
        QByteArray                      mReplyData;
        QPointer<ElprisetjustnuProxy>   mProxy;

};

#endif // PRICETIMELINEREQUEST_H
