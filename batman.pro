# ****************************************************************************
# Copyright 2024 Erik Ridderby,
# ARCHEA secondary business name to LIDEA AB
# 
# This file is part of BatMan - Battery Manager.
# 
# BatMan is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
# 
# BatMan is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License along
# with StuderProxy. If not, see <https://www.gnu.org/licenses/>.
# 
# ****************************************************************************

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Settings/usersettings.cpp \
    aboutdialog.cpp \
    gadgets/batterybar.cpp \
    gadgets/batterybarwidget.cpp \
    gadgets/lightmeeter.cpp \
    gadgets/lightmeeterform.cpp \
    main.cpp \
    mainwindow.cpp \
    modes/modescontroller.cpp \
    modes/modeviewcontroller.cpp \
    modes/persistentmode/persistentmode.cpp \
    modes/persistentmode/persistentmodeeditor.cpp \
    modes/persistentmode/persistentmodeviewer.cpp \
    modes/profile.cpp \
    modes/scheduledmode/scheduledmodeeditor.cpp \
    modes/scheduledmode/scheduledmodeviewer.cpp \
    modes/scheduledmode/scheduleitem.cpp \
    modes/scheduledmode/schedulemodel.cpp \
    pricediagram/elprisetjustnuproxy.cpp \
    pricediagram/pricediagram.cpp \
    pricediagram/pricediagramwidget.cpp \
    pricediagram/pricetimeline.cpp \
    pricediagram/pricetimelinerequest.cpp \
    pricediagram/pricetimeslot.cpp \
    pubsubclient.cpp

HEADERS += \
    Settings/usersettings.h \
    aboutdialog.h \
    gadgets/batterybar.h \
    gadgets/batterybarwidget.h \
    gadgets/lightmeeter.h \
    gadgets/lightmeeterform.h \
    mainwindow.h \
    modes/modescontroller.h \
    modes/modeviewcontroller.h \
    modes/persistentmode/persistentmode.h \
    modes/persistentmode/persistentmodeeditor.h \
    modes/persistentmode/persistentmodeviewer.h \
    modes/profile.h \
    modes/scheduledmode/scheduledmodeeditor.h \
    modes/scheduledmode/scheduledmodeviewer.h \
    modes/scheduledmode/scheduleitem.h \
    modes/scheduledmode/schedulemodel.h \
    pricediagram/elprisetjustnuproxy.h \
    pricediagram/pricediagram.h \
    pricediagram/pricediagramwidget.h \
    pricediagram/pricetimeline.h \
    pricediagram/pricetimelinerequest.h \
    pricediagram/pricetimeslot.h \
    pubsubclient.h

FORMS += \
    Settings/usersettings.ui \
    aboutdialog.ui \
    gadgets/batterybarwidget.ui \
    gadgets/lightmeeterform.ui \
    mainwindow.ui \
    modes/persistentmode/persistentmodeeditor.ui \
    modes/persistentmode/persistentmodeviewer.ui \
    modes/scheduledmode/scheduledmodeeditor.ui \
    modes/scheduledmode/scheduledmodeviewer.ui \
    pricediagram/pricediagramwidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    batman.qrc

