/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "pubsubclient.h"

#include <QIODevice>
#include <QJsonDocument>

PubSubClient::PubSubClient(const QString &host, int port, QObject *parent)
    : QObject{parent}
    , mHost {host}
    , mPort {port}
{
    mReconnectTimer.setInterval(60 * 1000);
    mReconnectTimer.stop();

    connect(&mReconnectTimer, &QTimer::timeout, this, &PubSubClient::connectToProxy);
    connect(&mSocket, &QTcpSocket::readyRead, this, &PubSubClient::readsocket);


}

void PubSubClient::connectToProxy()
{
    qDebug() << "PubSubClient, connecting to host";

    mSocket.connectToHost(mHost, mPort, QIODevice::ReadOnly | QIODevice::Text);
    mReconnectTimer.stop();

}

void PubSubClient::readsocket()
{

    QByteArray  rawdata = mSocket.readAll();
    if (rawdata.isEmpty())
        return;

    QVariantMap data = QJsonDocument::fromJson(rawdata).toVariant().toMap();
    emit dataRecevied(data);


}

void PubSubClient::errorOccurred(QAbstractSocket::SocketError socketError)
{
    qDebug() << "PubSubClient, Error occured: " << socketError;
}
