/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PUBSUBCLIENT_H
#define PUBSUBCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>

class PubSubClient : public QObject
{
        Q_OBJECT
    public:
        explicit PubSubClient(const QString& host, int port, QObject *parent = nullptr);

    public slots:
        void connectToProxy();


    protected slots:

        void readsocket();
        void errorOccurred(QAbstractSocket::SocketError socketError);


    signals:

        void dataRecevied(QVariantMap data);


    private:
        QString                 mHost;
        int                     mPort;
        QTcpSocket              mSocket;

        QTimer                  mReconnectTimer;

};

#endif // PUBSUBCLIENT_H
