/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef LIGHTMEETER_H
#define LIGHTMEETER_H

#include <QWidget>
#include <QVariant>

class LightMeeter : public QWidget
{
        Q_OBJECT
    public:
        explicit LightMeeter(QWidget *parent = nullptr);

        QVariant leftValue() const;
        QVariant rightValue() const;
        bool hasRightValue() const;
        bool hasLeftValue() const;

    public slots:

        void setLeftValue(QVariant value);
        void setLeftMaxValue(QVariant value);

        void setRightValue(QVariant value);
        void setRightMaxValue(QVariant value);

        void setSignedValue(QVariant value);
        void setSignedMaxValue(QVariant value);
        void setReverseSign(bool reverse=false);

    signals:

    protected:
        void paintEvent(QPaintEvent *event) override;

    protected:

        qreal       mLeftValue;
        qreal       mLeftMax;
        bool        bhasLeftValue;

        qreal       mRightValue;
        qreal       mRightMax;
        bool        bHasRightValue;

        bool        bReverse;


};

#endif // LIGHTMEETER_H
