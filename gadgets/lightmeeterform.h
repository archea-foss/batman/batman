/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef LIGHTMEETERFORM_H
#define LIGHTMEETERFORM_H

#include <QWidget>
#include <QVariant>

namespace Ui {
class LightMeeterForm;
}

class LightMeeterForm : public QWidget
{
        Q_OBJECT

    public:
        explicit LightMeeterForm(QWidget *parent = nullptr);
        ~LightMeeterForm();

    public slots:

        void setTitle(const QString& title);

        void setLeftValue(QVariant value);
        void setLeftMaxValue(QVariant value);
        void setLeftUnit(const QString& unit, qreal scale);

        void setRightValue(QVariant value);
        void setRightMaxValue(QVariant value);
        void setRightUnit(const QString& unit, qreal scale);

        void setSignedValue(QVariant value);
        void setSignedMaxValue(QVariant value);
        void setSignedUnit(const QString& unit, qreal scale);
        void setReverseSign(bool reverse=false);

    protected slots:
        void updateValueLabels();

    private:
        Ui::LightMeeterForm *ui;
        QString                 mLeftUnit;
        qreal                   mLeftScale;
        QString                 mRightUnit;
        qreal                   mRightScale;





};

#endif // LIGHTMEETERFORM_H
