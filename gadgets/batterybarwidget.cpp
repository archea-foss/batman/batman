/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "batterybarwidget.h"
#include "ui_batterybarwidget.h"

#include <QVariant>

BatteryBarWidget::BatteryBarWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BatteryBarWidget)
  , mScale(1)

{
    ui->setupUi(this);
}

BatteryBarWidget::~BatteryBarWidget()
{
    delete ui;
}

void BatteryBarWidget::setTitle(const QString &title)
{
    ui->pTitle->setText(title);
}

void BatteryBarWidget::setValue(QVariant newValue)
{
    ui->pMeeter->setValue(newValue);

    ui->pValueLabel->setText(QString().asprintf("%0.0f ", newValue.toDouble() / mScale) + mUnit);

}

void BatteryBarWidget::setMaxValue(QVariant newMaxValue)
{
    ui->pMeeter->setMaxValue(newMaxValue);

}

void BatteryBarWidget::setUnit(const QString &unit, qreal scale)
{
    mUnit = unit;
    mScale = scale;

}

