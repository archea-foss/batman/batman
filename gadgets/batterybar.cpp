/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "batterybar.h"

#include <QPaintEvent>
#include <QPainter>
#include <QPen>
#include <QRectF>

BatteryBar::BatteryBar(QWidget *parent)
    : QWidget{parent}
    , mValue(0)
    , mMaxValue(1)
{
    setWindowTitle(tr("Meteer"));
    resize(40, 200);

}

qreal BatteryBar::value() const
{
    return mValue;
}

void BatteryBar::setValue(QVariant newValue)
{
    mValue = newValue.toDouble();
    update();
}

qreal BatteryBar::maxValue() const
{
    return mMaxValue;
}

void BatteryBar::setMaxValue(QVariant newMaxValue)
{
    mMaxValue = newMaxValue.toDouble();
    update();
}

void BatteryBar::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing);


    // Draw the background bar

    QPen    baseLinePen(Qt::darkGreen);
    baseLinePen.setCapStyle(Qt::FlatCap);

    painter.setPen(baseLinePen);
    painter.drawLine(20, 10, 20, 100);

    // Draw the value-bar

    QPen    rightValuePen(Qt::green);
    rightValuePen.setWidth(10);
    rightValuePen.setCapStyle(Qt::FlatCap);
    painter.setPen(rightValuePen);

    painter.drawLine(20, 10 + 90 - 90 * mValue / mMaxValue, 20, 100);


    event->accept();


}
