/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "lightmeeter.h"

#include <QPaintEvent>
#include <QPainter>
#include <QRectF>

#include <QtDebug>
LightMeeter::LightMeeter(QWidget *parent)
    : QWidget{parent}
    , mLeftValue(0)
    , mLeftMax(1)
    , bhasLeftValue(true)
    , mRightValue(0)
    , mRightMax(1)
    , bHasRightValue(true)
    , bReverse(false)
{
    setWindowTitle(tr("Meteer"));
    resize(400, 200);
}

void LightMeeter::setLeftValue(QVariant value)
{
    // qDebug() << "Left Value: " << value;
    mLeftValue = value.toDouble();
    bhasLeftValue = true;
    update();

}

void LightMeeter::setLeftMaxValue(QVariant value)
{
    mLeftMax = value.toDouble();
    update();

}

void LightMeeter::setRightValue(QVariant value)
{
    // qDebug() << "Right Value: " << value;
    mRightValue = value.toDouble();
    bHasRightValue = true;
    update();
}

void LightMeeter::setRightMaxValue(QVariant value)
{
    mRightMax = value.toDouble();
    update();

}

void LightMeeter::setSignedValue(QVariant value)
{
    // qDebug() << "Signed Value: " << value;
    qreal rValue = value.toDouble();

    if (bReverse)
        rValue = 0 - rValue;


    if (rValue >= 0)
    {
        mRightValue = rValue;
        bHasRightValue = true;
        bhasLeftValue = false;
    }
    else
    {
        mLeftValue = 0 - rValue;
        bHasRightValue = false;
        bhasLeftValue = true;
    }
    update();

}

void LightMeeter::setSignedMaxValue(QVariant value)
{
    mRightMax = value.toDouble();
    mLeftMax  = mRightMax;
    update();
}

void LightMeeter::setReverseSign(bool reverse)
{
    bReverse = reverse;
    update();

}

void LightMeeter::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing);

    // Draw the left arch.

    QRectF archFrame(10, 10, 180, 180);


    QPen    leftLinePen(Qt::darkRed);

    painter.setPen(leftLinePen);
    painter.drawArc(archFrame, 90*16, 90*16);

    if (bhasLeftValue)
    {
        QPen    leftValuePen(Qt::red);
        leftValuePen.setWidth(10);
        leftValuePen.setCapStyle(Qt::FlatCap);

        painter.setPen(leftValuePen);
        painter.drawArc(archFrame, 90*16, (90 * mLeftValue / mLeftMax)*16);
    }

//    QRectF rightArchFrame(190, 10, 180, 180);

    QPen    rightLinePen(Qt::darkGreen);

    painter.setPen(rightLinePen);
    painter.drawArc(archFrame, 90*16, -90*16);

    if (bHasRightValue)
    {
        QPen    rightValuePen(Qt::green);
        rightValuePen.setWidth(10);
        rightValuePen.setCapStyle(Qt::FlatCap);

        painter.setPen(rightValuePen);


        qreal rightArch = (90 * mRightValue / mRightMax)*16;

        painter.drawArc(archFrame, 90*16 - rightArch, rightArch);
    }

    event->setAccepted(true);

}

bool LightMeeter::hasLeftValue() const
{
    return bhasLeftValue;
}

bool LightMeeter::hasRightValue() const
{
    return bHasRightValue;
}

QVariant LightMeeter::rightValue() const
{
    return mRightValue;
}

QVariant LightMeeter::leftValue() const
{
    return mLeftValue;
}
