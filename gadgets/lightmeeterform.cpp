/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "lightmeeterform.h"
#include "ui_lightmeeterform.h"

LightMeeterForm::LightMeeterForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LightMeeterForm)
  , mLeftScale(1)
  , mRightScale(1)
{
    ui->setupUi(this);

}

LightMeeterForm::~LightMeeterForm()
{
    delete ui;
}

void LightMeeterForm::setTitle(const QString &title)
{
    ui->pTitle->setText(title);

}

void LightMeeterForm::setLeftValue(QVariant value)
{
    ui->pMeeter->setLeftValue(value);
    updateValueLabels();
}

void LightMeeterForm::setLeftMaxValue(QVariant value)
{
    ui->pMeeter->setLeftMaxValue(value);
    updateValueLabels();
}

void LightMeeterForm::setLeftUnit(const QString &unit, qreal scale)
{
    mLeftUnit = unit;
    mLeftScale = scale;

}

void LightMeeterForm::setRightValue(QVariant value)
{
    ui->pMeeter->setRightValue(value);
    updateValueLabels();
}

void LightMeeterForm::setRightMaxValue(QVariant value)
{
    ui->pMeeter->setRightMaxValue(value);
    updateValueLabels();
}

void LightMeeterForm::setRightUnit(const QString &unit, qreal scale)
{
    mRightUnit = unit;
    mRightScale = scale;

}

void LightMeeterForm::setSignedValue(QVariant value)
{
    ui->pMeeter->setSignedValue(value);
    updateValueLabels();
}

void LightMeeterForm::setSignedMaxValue(QVariant value)
{
    ui->pMeeter->setSignedMaxValue(value);
    updateValueLabels();
}

void LightMeeterForm::setSignedUnit(const QString &unit, qreal scale)
{
    mLeftUnit = unit;
    mLeftScale = scale;
    mRightUnit = unit;
    mRightScale = scale;
}

void LightMeeterForm::setReverseSign(bool reverse)
{
    ui->pMeeter->setReverseSign(reverse);
    updateValueLabels();
}

void LightMeeterForm::updateValueLabels()
{
    if (ui->pMeeter->hasLeftValue())
        ui->pLeftValueLabel->setText(QString().asprintf("%0.1f ", ui->pMeeter->leftValue().toDouble() / mLeftScale) + mLeftUnit);
    else
        ui->pLeftValueLabel->clear();

    if (ui->pMeeter->hasRightValue())
        ui->pRightValueLabel->setText(QString().asprintf("%0.1f ", ui->pMeeter->rightValue().toDouble() / mRightScale) + mRightUnit);
    else
        ui->pRightValueLabel->clear();

}
