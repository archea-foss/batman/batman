/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef BATTERYBAR_H
#define BATTERYBAR_H

#include <QWidget>
#include <QVariant>

class BatteryBar : public QWidget
{
        Q_OBJECT
    public:
        explicit BatteryBar(QWidget *parent = nullptr);

        qreal value() const;

        qreal maxValue() const;

    public slots:
        void setValue(QVariant newValue);
        void setMaxValue(QVariant newMaxValue);

    signals:

    protected:
        void paintEvent(QPaintEvent *event) override;

    protected:

        qreal           mValue;
        qreal           mMaxValue;

};

#endif // BATTERYBAR_H
