/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "modescontroller.h"

#include "modeviewcontroller.h"

#include <QJsonDocument>

ModesController::ModesController(const QString &host, int port, ModeViewController* viewController, QObject *parent)
    : QObject{parent}
    , mHost {host}
    , mPort {port}
    , mViewController {viewController}
{
    connect(&mSocket, &QTcpSocket::readyRead, this, &ModesController::readsocket);
    connect(&mSocket, &QTcpSocket::errorOccurred, this, &ModesController::errorOccurred);

}

PersistentModeEditor *ModesController::persistentModeEditor(QWidget *parent)
{
    PersistentModeEditor* editor = new PersistentModeEditor(&mProfiles, parent);
    connect(editor, &PersistentModeEditor::modeApplied, this, &ModesController::sendAppliedMode);

    return editor;

}

ScheduledModeEditor *ModesController::scheduledModeEditor(QWidget *parent)
{
    ScheduledModeEditor* editor = new ScheduledModeEditor(&mProfiles, parent);    
    connect(editor, &ScheduledModeEditor::modeApplied, this, &ModesController::sendAppliedMode);
    connect(this, &ModesController::modeApplied, editor, &ScheduledModeEditor::applyModeSettingsDisableEditor);

    editor->applyModeSettings(mLastModeRecevied);

    return editor;

}

void ModesController::connectManager()
{

    qDebug() << "ModesController, connecting to host";

    mSocket.connectToHost(mHost, mPort, QIODevice::ReadWrite | QIODevice::Text);

}

void ModesController::readProfileSettings(QVariantMap profiles)
{
    foreach(QVariant item, profiles.value("Profiles").toList())
    {
        Profile* newProfile = new Profile(item.toMap(), this);
        mProfiles.insert(newProfile->mName, newProfile);

        qDebug() << newProfile->mName  << Qt::endl
                 << newProfile->mColor << Qt::endl
                 << newProfile->bIsDefault;

    }

}

void ModesController::readModeApplied(QVariantMap modeSettings)
{
    mLastModeRecevied = modeSettings;

    if (mViewController)
        mViewController->appliedMode(mLastModeRecevied);

    emit modeApplied(mLastModeRecevied);

}

void ModesController::sendAppliedMode(QVariantMap modeSettings)
{
    qDebug() << "Applies mode " << modeSettings << " to server";

    mSocket.write(QJsonDocument::fromVariant(modeSettings).toJson());

}

void ModesController::readsocket()
{

    QJsonParseError error;
    QByteArray raw = mSocket.readAll();
    QList<QByteArray> inputs = raw.split(QChar::FormFeed);


    foreach(QByteArray inputline, inputs)
    {

        inputline.replace(QChar::FormFeed, QByteArray());

        if (inputline.size() > 0)
        {


            QVariantMap input = QJsonDocument::fromJson(inputline, & error).toVariant().toMap();


            qDebug() << "ModesController got input" << Qt::endl
                     << "    Raw    : " << inputline << Qt::endl
                     << "    Data   : " << input << Qt::endl
                     << "    Error  : " << error.errorString() << Qt::endl
                     << "    Ofset  : " << error.offset;


            if (!input.isEmpty())
            {

                if (input.contains("Welcome"))
                {
                    qDebug() << "Welcomed to the host";
                }

                if (input.contains("Profile Settings"))
                {
                    qDebug() << "Got profileSettings:";
                    readProfileSettings(input.value("Profile Settings").toMap());
                }
                if (input.contains("Mode Applied"))
                {
                    qDebug() << "Got mode applied:";
                    readModeApplied(input.value("Mode Applied").toMap());
                }

            }
        }
        else
        {
            qDebug() << "Got an empty input that is isgnored";
        }

    }
}

void ModesController::errorOccurred(QAbstractSocket::SocketError socketError)
{

}
