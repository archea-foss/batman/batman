/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "modeviewcontroller.h"
#include "persistentmode/persistentmodeviewer.h"
#include "scheduledmode/scheduledmodeviewer.h"

#include <QVariant>
#include <QVBoxLayout>

#include <QtDebug>

ModeViewController::ModeViewController(QWidget *parent)
    : QWidget{parent}
{
    setLayout(new QVBoxLayout(this));

}

void ModeViewController::appliedMode(QVariantMap modeSettings)
{

    if (modeSettings.value("Mode").toString() == "Persistent Mode")
    {
        qDebug() << "ModeViewController: Persistent Mode applied";
        applyPersistentMode(modeSettings);
    }
    else if (modeSettings.value("Mode").toString() == "Scheduled Mode")
    {
        qDebug() << "ModeViewController: Scheduled Mode applied";
        applyScheduledMode(modeSettings);
    }

}

void ModeViewController::applyPersistentMode(QVariantMap modeSettings)
{

    if (mCurrentWidget)
    {
        int index = layout()->indexOf(mCurrentWidget);
        QLayoutItem* tmp = layout()->takeAt(index);

        if (tmp)
        {
            tmp->widget()->deleteLater();
            delete tmp;
        }
    }

    mCurrentWidget = new PersistentModeViewer(modeSettings, this);
    layout()->addWidget(mCurrentWidget);

}

void ModeViewController::applyScheduledMode(QVariantMap modeSettings)
{
    if (mCurrentWidget)
    {
        int index = layout()->indexOf(mCurrentWidget);
        QLayoutItem* tmp = layout()->takeAt(index);

        if (tmp)
        {
            tmp->widget()->deleteLater();
            delete tmp;
        }
    }

    mCurrentWidget = new ScheduledModeViewer(modeSettings, this);
    layout()->addWidget(mCurrentWidget);
}
