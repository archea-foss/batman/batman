/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "persistentmodeeditor.h"
#include "ui_persistentmodeeditor.h"

#include <QAbstractButton>
#include <QDateTime>
#include <QDateTime>
#include <QPushButton>

#include <QtDebug>

PersistentModeEditor::PersistentModeEditor(ProfileMap *profiles, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::PersistentModeEditor)
{
    ui->setupUi(this);

    mProfileNames = profiles->keys();
    ui->pProfiles->insertItems(0, mProfileNames);


    connect(ui->pBtnBox, &QDialogButtonBox::clicked, this, &PersistentModeEditor::btnBoxBroker);
}

PersistentModeEditor::~PersistentModeEditor()
{
    delete ui;
}

void PersistentModeEditor::applyPressed()
{
    int index = ui->pProfiles->currentIndex();
    QString profileName = mProfileNames.at(index);
    qDebug() << "Applies Persistent mode using the " << profileName << " profile";

    QVariantMap applyMode;
    QVariantMap modeInfo;

    modeInfo.insert("Mode", "Persistent Mode");
    modeInfo.insert("Profile", profileName);
    modeInfo.insert("Timestamp", QDateTime::currentDateTime());

    applyMode.insert("Apply Mode", modeInfo);


    emit modeApplied(applyMode);

}

void PersistentModeEditor::applyModeSettings(QVariantMap modeSettings)
{

}


void PersistentModeEditor::btnBoxBroker(QAbstractButton *button)
{

    if (ui->pBtnBox->button(QDialogButtonBox::Apply) == button)
    {
        this->applyPressed();
    }
    else if (ui->pBtnBox->button(QDialogButtonBox::Close) == button)
    {
        this->close();
    }

}
