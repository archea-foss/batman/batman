/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PERSISTENTMODE_H
#define PERSISTENTMODE_H

#include <QDateTime>
#include <QObject>

#include "modes/profile.h"

class PersistentMode : public QObject
{
        Q_OBJECT
    public:
        explicit PersistentMode(QVariantMap settings, const ProfileMap& profileMap, QObject *parent = nullptr);

    signals:

    protected:

        ProfilePointer          mProfile;
        QDateTime               mTimeStamp;


};

#endif // PERSISTENTMODE_H
