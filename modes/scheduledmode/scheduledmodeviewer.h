#ifndef SCHEDULEDMODEVIEWER_H
#define SCHEDULEDMODEVIEWER_H

#include <QWidget>

#include "schedulemodel.h"

namespace Ui {
class ScheduledModeViewer;
}

class ScheduledModeViewer : public QWidget
{
        Q_OBJECT

    public:
        explicit ScheduledModeViewer(QVariantMap modeSettings, QWidget *parent = nullptr);
        ~ScheduledModeViewer();

    public slots:

        void applyModeSettings(QVariantMap modeSettings);

    private:
        Ui::ScheduledModeViewer *ui;

        ScheduleModel       mModel;

};

#endif // SCHEDULEDMODEVIEWER_H
