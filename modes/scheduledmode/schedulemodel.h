#ifndef SCHEDULEMODEL_H
#define SCHEDULEMODEL_H

#include "scheduleitem.h"

#include <QAbstractListModel>

class ScheduleModel : public QAbstractListModel
{
        Q_OBJECT

    public:
        explicit ScheduleModel(QObject *parent = nullptr);
        virtual ~ScheduleModel();

        virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
        virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;

        virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

        virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        void clear();
        void append(ScheduleItem* item);
        void remove(ScheduleItem* item);
        ScheduleItem* itemAt(const QModelIndex &index);
        void itemChanged(ScheduleItem* item);

        QList<ScheduleItem *> items() const;

    signals:

        void itemSelected(ScheduleItem* item);


    private:

        QList<ScheduleItem*>        mItems;
};

#endif // SCHEDULEMODEL_H
