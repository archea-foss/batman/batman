#ifndef SCHEDULEDMODEEDITOR_H
#define SCHEDULEDMODEEDITOR_H

#include "modes/profile.h"

#include "schedulemodel.h"

#include <QDialog>

namespace Ui {
class ScheduledModeEditor;
}

class ScheduledModeEditor : public QDialog
{
        Q_OBJECT

    public:
        explicit ScheduledModeEditor(ProfileMap* profiles, QWidget *parent = nullptr);
        ~ScheduledModeEditor();

    public slots:

        void applyModeSettings(QVariantMap modeSettings, bool disable = false);
        void applyModeSettingsDisableEditor(QVariantMap modeSettings);

    protected slots:

        void btnAppendPressed();
        void btnUpdatePressed();
        void btnRemovePressed();
        void btnResetePressed();

        void dialogBtnApplyPressed();

        void indexActivated(const QModelIndex &index);

        void disableButtons();
        void enableButtons();
        void disableEditing();
        void enableEditing();
    signals:

        void modeApplied(QVariantMap mode);

    protected:

        bool checkScheule();
        virtual void showEvent(QShowEvent *event)   override;

    private:
        Ui::ScheduledModeEditor *ui;
        QStringList             mProfileNames;
        ScheduleModel           mModel;
        ScheduleItem            *mCurrentItem;
};

#endif // SCHEDULEDMODEEDITOR_H
