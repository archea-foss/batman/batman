#include "schedulemodel.h"

#include <QDate>
#include <QFont>
#include <QVariant>

ScheduleModel::ScheduleModel(QObject *parent)
    : QAbstractListModel{parent}
{

}

ScheduleModel::~ScheduleModel()
{

}

int ScheduleModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return mItems.count();

}

int ScheduleModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant ScheduleModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant null;

    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch(section)
        {
            case 0:
                return "Date";
            case 1:
                return "Time";
            case 2:
                return "Profile";

            default:
                return null;
        }
    }
    return null;

}

QVariant ScheduleModel::data(const QModelIndex &index, int role) const
{
    QVariant null;

    if (Qt::DisplayRole == role)
    {
        if (index.row() >= 0 && index.row() < mItems.count())
        {

            ScheduleItem    *item = mItems.at(index.row());

            if (item)
            {

                QLocale     locale;
                switch(index.column())
                {
                    case 0:
                        if (item->scheduledAt.date() == QDate::currentDate())
                            return "Today";
                        else if (item->scheduledAt.date() == QDate::currentDate().addDays(1))
                            return "Tomorrow";
                        else
                            return locale.toString(item->scheduledAt.date(), "yy-MM-dd");
                            // return item->scheduledAt.date().toString();
                    case 1:
                        return locale.toString(item->scheduledAt.time(), "hh:mm");
                        // return item->scheduledAt.time().toString();
                    case 2:
                        return item->profileName;

                    default:
                        return null;
                }
            }
        }
    }
    else if (Qt::FontRole == role)
    {
        ScheduleItem    *item = mItems.at(index.row());

        if (item && item->isActive && index.column() <= 2)
        {
            QFont font;

            font.setBold(true);

            return font;
        }
        else
        {
            return null;
        }

    }
    return null;

}

void ScheduleModel::clear()
{
    beginResetModel();

    while(!mItems.isEmpty())
        delete mItems.takeFirst();

    endResetModel();
}


void ScheduleModel::append(ScheduleItem *item)
{
    beginInsertRows(QModelIndex(), mItems.count(), mItems.count());

    mItems.append(item);

    endInsertRows();

    // Sort the list.

    emit layoutAboutToBeChanged({QModelIndex()}, QAbstractItemModel::VerticalSortHint);

    std::sort(mItems.begin(), mItems.end(),
              [](const ScheduleItem* a, const ScheduleItem* b)->bool
                {
                        return a->scheduledAt < b->scheduledAt;
                });

    emit layoutChanged({QModelIndex()}, QAbstractItemModel::VerticalSortHint);

}

void ScheduleModel::remove(ScheduleItem *item)
{
    int index = mItems.indexOf(item);

    if (index >= 0 && index < mItems.count())
    {
        beginRemoveRows(QModelIndex(), index, index);
        delete mItems.takeAt(index);
        endRemoveRows();
    }
}

ScheduleItem *ScheduleModel::itemAt(const QModelIndex &index)
{
    if (index.isValid() && index.row() >= 0 && index.row() < mItems.count())
        return mItems.at(index.row());
    else
        return nullptr;

}

void ScheduleModel::itemChanged(ScheduleItem *item)
{
    if (item)
    {
        QList<int> roles = { Qt::DisplayRole };
        int itemIndex = mItems.indexOf(item);

        QModelIndex uppLeft = createIndex(itemIndex, 0);
        QModelIndex downRight = createIndex(itemIndex, 3);

        emit dataChanged(uppLeft, downRight, roles);

        // Sort the list.

        emit layoutAboutToBeChanged({QModelIndex()}, QAbstractItemModel::VerticalSortHint);

        std::sort(mItems.begin(), mItems.end(),
                  [](const ScheduleItem* a, const ScheduleItem* b)->bool
                    {
                            return a->scheduledAt < b->scheduledAt;
                    });

        emit layoutChanged({QModelIndex()}, QAbstractItemModel::VerticalSortHint);
    }

}

QList<ScheduleItem *> ScheduleModel::items() const
{
    return mItems;
}
