#ifndef SCHEDULEITEM_H
#define SCHEDULEITEM_H

#include <QDateTime>


struct ScheduleItem
{
        ScheduleItem();


        QDateTime       scheduledAt;
        QString         profileName;
        bool            isActive;

};

#endif // SCHEDULEITEM_H
