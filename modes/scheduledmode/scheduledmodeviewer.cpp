#include "scheduledmodeviewer.h"
#include "ui_scheduledmodeviewer.h"

ScheduledModeViewer::ScheduledModeViewer(QVariantMap modeSettings, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScheduledModeViewer)
{
    ui->setupUi(this);
    ui->pAppliedSchedule->setModel(&mModel);
    applyModeSettings(modeSettings);
}

ScheduledModeViewer::~ScheduledModeViewer()
{
    delete ui;
}

void ScheduledModeViewer::applyModeSettings(QVariantMap modeSettings)
{
    QLocale     locale;

    QString profileName = modeSettings.value("Profile").toString();
    QString onSince = locale.toString(modeSettings.value("Timestamp").toDateTime());

    ui->pProfileLabel->setText(profileName);
    ui->pTimeStamp->setText(onSince);

    mModel.clear();

    foreach(QVariant vitem, modeSettings.value("Schedule").toList())
    {
        QVariantMap scheduleInfo = vitem.toMap();
        ScheduleItem* item = new ScheduleItem;

        item->profileName = scheduleInfo.value("Profile").toString();
        item->scheduledAt = scheduleInfo.value("Scheduled At").toDateTime();
        item->isActive    = scheduleInfo.value("Is active", false).toBool();

        mModel.append(item);

    }

}
