#include "scheduledmodeeditor.h"
#include "ui_scheduledmodeeditor.h"

#include <QJsonDocument>
#include <QMessageBox>
#include <QTimer>
#include <QtDebug>


ScheduledModeEditor::ScheduledModeEditor(ProfileMap *profiles, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScheduledModeEditor)
{
    ui->setupUi(this);
    mProfileNames = profiles->keys();

    ui->pProfileList->insertItems(0, mProfileNames);

    ui->pScheduledList->setModel(&mModel);

    connect(ui->pBtnAppend, &QPushButton::clicked, this, &ScheduledModeEditor::btnAppendPressed);
    connect(ui->pBtnUpdate, &QPushButton::clicked, this, &ScheduledModeEditor::btnUpdatePressed);
    connect(ui->pBtnRemove, &QPushButton::clicked, this, &ScheduledModeEditor::btnRemovePressed);
    connect(ui->pBtnReset, &QPushButton::clicked, this, &ScheduledModeEditor::btnResetePressed);
    connect(ui->pScheduledList, &QTreeView::activated, this, &ScheduledModeEditor::indexActivated);

    connect(ui->pBtnBox->button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &ScheduledModeEditor::dialogBtnApplyPressed);
    connect(ui->pBtnBox->button(QDialogButtonBox::Close), &QPushButton::clicked, this, &ScheduledModeEditor::close);

    ui->pBtnUpdate->setEnabled(false);

}

ScheduledModeEditor::~ScheduledModeEditor()
{
    delete ui;
}

void ScheduledModeEditor::applyModeSettings(QVariantMap modeSettings, bool disable)
{
    if (modeSettings.value("Mode").toString() == "Scheduled Mode")
    {

        ui->pScheduledList->clearSelection();
        disableButtons();


        mModel.clear();

        foreach(QVariant vitem, modeSettings.value("Schedule").toList())
        {
            QVariantMap scheduleInfo = vitem.toMap();
            ScheduleItem* item = new ScheduleItem;

            item->profileName = scheduleInfo.value("Profile").toString();
            item->scheduledAt = scheduleInfo.value("Scheduled At").toDateTime();
            item->isActive    = scheduleInfo.value("Is active", false).toBool();

            mModel.append(item);

        }

        if (disable)
        {
            disableEditing();
            QTimer::singleShot(3000, this, &ScheduledModeEditor::enableEditing);
        }
    }

}

void ScheduledModeEditor::applyModeSettingsDisableEditor(QVariantMap modeSettings)
{
    applyModeSettings(modeSettings, true);

}

void ScheduledModeEditor::btnAppendPressed()
{

    if (checkScheule())
    {
        ScheduleItem* item = new ScheduleItem;

        item->scheduledAt = QDateTime(ui->pDate->date(), ui->pTime->time());
        item->profileName = ui->pProfileList->currentText();

        mModel.append(item);
    }

}

void ScheduledModeEditor::btnUpdatePressed()
{
    QDateTime       scheduledTime(ui->pDate->date(), ui->pTime->time());

    if (mCurrentItem && checkScheule())
    {
        mCurrentItem->scheduledAt = scheduledTime;
        mCurrentItem->profileName = ui->pProfileList->currentText();

        mModel.itemChanged(mCurrentItem);
    }
}

void ScheduledModeEditor::btnRemovePressed()
{
    if (mCurrentItem)
    {
        disableButtons();

        mModel.remove(mCurrentItem);

        QItemSelectionModel* selections = ui->pScheduledList->selectionModel();
        if (selections && selections->currentIndex().isValid())
        {
            indexActivated(selections->currentIndex());
        }

    }
}

void ScheduledModeEditor::btnResetePressed()
{
    if (mCurrentItem)
    {
        ui->pDate->setDate( mCurrentItem->scheduledAt.date());
        ui->pTime->setTime( mCurrentItem->scheduledAt.time());
        if (mProfileNames.contains(mCurrentItem->profileName))
            ui->pProfileList->setCurrentIndex(mProfileNames.indexOf(mCurrentItem->profileName));
    }
}

void ScheduledModeEditor::dialogBtnApplyPressed()
{
    qDebug() << "Applie Scheduled Mode";

    QVariantMap     applyMode;
    QVariantMap     modeInfo;
    QVariantList    modeList;


    foreach(ScheduleItem* item, mModel.items())
    {
        if (item)
        {
            QVariantMap itemMap;

            itemMap.insert("Scheduled At", item->scheduledAt);
            itemMap.insert("Profile", item->profileName);

            modeList.append(itemMap);
        }
    }

    modeInfo.insert("Mode", "Scheduled Mode");
    modeInfo.insert("Schedule", modeList);

    applyMode.insert("Apply Mode", modeInfo);

    qDebug() << "Mode to apply: ";
    qDebug().noquote() << QString::fromUtf8(QJsonDocument::fromVariant(applyMode).toJson());

    emit modeApplied(applyMode);

}

void ScheduledModeEditor::indexActivated(const QModelIndex &index)
{
    qDebug() << "index activated " << index.row();

    ScheduleItem* item = mModel.itemAt(index);
    if (item)
    {
        mCurrentItem = item;
        enableButtons();

        ui->pDate->setDate( item->scheduledAt.date());
        ui->pTime->setTime( item->scheduledAt.time());
        if (mProfileNames.contains(item->profileName))
            ui->pProfileList->setCurrentIndex(mProfileNames.indexOf(item->profileName));
    }
    else
    {
        mCurrentItem = nullptr;
        disableButtons();

    }
}

void ScheduledModeEditor::disableButtons()
{
    ui->pBtnRemove->setEnabled(false);
    ui->pBtnUpdate->setEnabled(false);
    ui->pBtnReset->setEnabled(false);
}

void ScheduledModeEditor::enableButtons()
{
    ui->pBtnRemove->setEnabled(true);
    ui->pBtnUpdate->setEnabled(true);
    ui->pBtnReset->setEnabled(true);
}

void ScheduledModeEditor::disableEditing()
{

    ui->pGrpEditor->setEnabled(false);
    ui->pGrpScheduleList->setEnabled(false);

    if (ui->pBtnBox->button(QDialogButtonBox::Apply))
        ui->pBtnBox->button(QDialogButtonBox::Apply)->setEnabled(false);

}

void ScheduledModeEditor::enableEditing()
{
    ui->pGrpEditor->setEnabled(true);
    ui->pGrpScheduleList->setEnabled(true);

    if (ui->pBtnBox->button(QDialogButtonBox::Apply))
        ui->pBtnBox->button(QDialogButtonBox::Apply)->setEnabled(true);
}

bool ScheduledModeEditor::checkScheule()
{
    QDateTime       scheduledTime(ui->pDate->date(), ui->pTime->time());

    foreach(ScheduleItem* item, mModel.items())
    {
        if (item && item != mCurrentItem && item->scheduledAt == scheduledTime)
        {
            QMessageBox::critical(this, "Schedule Error",
                                  "Cannot have more than one schedule at the same time.\n"
                                  "Please adjust the time or date of the schedule and try again",
                                  QMessageBox::Ok,
                                  QMessageBox::Ok);
            return false;
        }
    }

    return true;
}


void ScheduledModeEditor::showEvent(QShowEvent *event)
{
    if (event)
    {
        ui->pDate->setDate(QDate::currentDate());

        QTime  now = QTime::currentTime();
        now.setHMS(QTime::currentTime().hour(), QTime::currentTime().minute(), 0, 0);

        ui->pTime->setTime(now);
    }

    QDialog::showEvent(event);
}
