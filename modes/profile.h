/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PROFILE_H
#define PROFILE_H

#include <QObject>
#include <QColor>
#include <QVariant>
#include <QPointer>

class Profile;

typedef QList< QPointer<Profile>>           ProfileList;
typedef QPointer<Profile>                   ProfilePointer;
typedef QMap<QString, QPointer<Profile>>    ProfileMap;

class Profile : public QObject
{
        Q_OBJECT

    public:

        explicit Profile(QVariantMap map, QObject* parent = nullptr);

        bool        bIsDefault;
        QString     mName;
        QColor      mColor;


};

#endif // PROFILE_H
