/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODEVIEWCONTROLLER_H
#define MODEVIEWCONTROLLER_H

#include <QPointer>
#include <QWidget>
#include <QVariant>

class ModeViewController : public QWidget
{
        Q_OBJECT
    public:
        explicit ModeViewController(QWidget *parent = nullptr);

    public slots:

        void appliedMode(QVariantMap modeSettings);

    protected slots:

        void applyPersistentMode(QVariantMap modeSettings);
        void applyScheduledMode(QVariantMap modeSettings);
    signals:

    private:

        QPointer<QWidget>       mCurrentWidget;

};

#endif // MODEVIEWCONTROLLER_H
