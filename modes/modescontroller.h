/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODESCONTROLLER_H
#define MODESCONTROLLER_H

#include "modeviewcontroller.h"
#include "persistentmode/persistentmode.h"
#include "persistentmode/persistentmodeeditor.h"
#include "scheduledmode/scheduledmodeeditor.h"
#include "profile.h"

#include <QObject>
#include <QPointer>
#include <QTcpSocket>

class ModeViewController;
class ModesController : public QObject
{
        Q_OBJECT
    public:
        explicit ModesController(const QString& host, int port, ModeViewController *viewController, QObject *parent = nullptr);

        PersistentModeEditor* persistentModeEditor(QWidget* parent = nullptr);
        ScheduledModeEditor*  scheduledModeEditor(QWidget* parent = nullptr);

    public slots:

        void connectManager();

    protected slots:

        void readProfileSettings(QVariantMap profiles);
        void readModeApplied(QVariantMap modeSettings);

        void sendAppliedMode(QVariantMap modeSettings);

        void readsocket();
        void errorOccurred(QAbstractSocket::SocketError socketError);
    signals:

        void modeApplied(QVariantMap modeSettings);


    private:

        QString                             mHost;
        int                                 mPort;

        QTcpSocket                          mSocket;
        QPointer<ModeViewController>        mViewController;
        ProfileMap                          mProfiles;
        QVariantMap                         mLastModeRecevied;

};

#endif // MODESCONTROLLER_H
