/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef USERSETTINGS_H
#define USERSETTINGS_H

#include <QDialog>
#include <QSettings>
#include <QVariant>

namespace Ui {
class UserSettings;
}

class UserSettings : public QDialog
{
        Q_OBJECT

    public:
        explicit UserSettings(QWidget *parent = nullptr);
        ~UserSettings();

        QString daemonHost() const;
        QVariant pubSubPort() const;
        QVariant managerPort() const;

    public slots:

        void checkAndLoadSettings();
        void writebackSettings();

    protected slots:

        void btnOkPressed();


    signals:

        void needToShowSettingsDialog();
        void settingsLoaded();


    private:
        Ui::UserSettings *ui;

        QSettings       mSettings;

        QString         mDaemonHost;
        QVariant        mPubSubPort;
        QVariant        mManagerPort;

};

#endif // USERSETTINGS_H
