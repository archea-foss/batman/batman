/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan - Battery Manager.

BatMan is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "usersettings.h"
#include "ui_usersettings.h"

#include <QSettings>
#include <QPushButton>

#include <QtDebug>

UserSettings::UserSettings(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::UserSettings)
    , mSettings {"archea", "batman console"}
{
    ui->setupUi(this);
    ui->pDaemonHost->setFocus();
    connect(ui->buttonBox->button(QDialogButtonBox::Ok), &QPushButton::clicked, this, &UserSettings::btnOkPressed);


    // checkAndLoadSettings();

}

UserSettings::~UserSettings()
{
    delete ui;
}

void UserSettings::checkAndLoadSettings()
{

    qDebug() << "Check and load settings";

    QStringList keys = mSettings.allKeys();

    QSet<QString>     keySet;
    QSet<QString>     needSet;

    foreach(QString str, keys)
        keySet.insert(str);

    needSet << "DaemonHost" << "PubSubPort" << "ManagerPort";

    qDebug() << "intersect: " << needSet.subtract(keySet);
    qDebug() << "result: " << needSet;


    if (mSettings.contains("DaemonHost"))
    {
        mDaemonHost = mSettings.value("DaemonHost").toString();
        ui->pDaemonHost->setText(mDaemonHost);
    }

    if (mSettings.contains("PubSubPort"))
    {
        mPubSubPort = mSettings.value("PubSubPort");
        ui->pPubSubPort->setText(mPubSubPort.toString());
    }

    if (mSettings.contains("ManagerPort"))
    {
        mManagerPort = mSettings.value("ManagerPort");
        ui->pManagerPort->setText(mManagerPort.toString());
    }

    if (!needSet.isEmpty())
    {
        emit needToShowSettingsDialog();
    }
    else
    {
        emit settingsLoaded();
    }
}

void UserSettings::writebackSettings()
{

    mSettings.setValue("DaemonHost", mDaemonHost);
    mSettings.setValue("PubSubPort", mPubSubPort);
    mSettings.setValue("ManagerPort", mManagerPort);

}

void UserSettings::btnOkPressed()
{
    mDaemonHost = ui->pDaemonHost->text();
    mPubSubPort = ui->pPubSubPort->text();
    mManagerPort = ui->pManagerPort->text();

    writebackSettings();

    if (!mDaemonHost.isEmpty()
            && mPubSubPort.canConvert<int>() && mPubSubPort.toInt() > 0
            && mManagerPort.canConvert<int>() && mManagerPort.toInt() > 0
        )
        emit settingsLoaded();

}

QVariant UserSettings::managerPort() const
{
    return mManagerPort;
}

QVariant UserSettings::pubSubPort() const
{
    return mPubSubPort;
}

QString UserSettings::daemonHost() const
{
    return mDaemonHost;
}
